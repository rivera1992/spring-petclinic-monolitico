FROM openjdk:11-jre-slim
WORKDIR /workspace
COPY target/spring-petclinic-*.jar app.jar
EXPOSE 9966
ENTRYPOINT [ "java", "-jar", "/workspace/app.jar" ]
#spring-petclinic-monolitico

#despliegue en dockerhub
#docker tag 57c melvin2rm/spring-petclinic-monolitico
#docker push melvin2rm/spring-petclinic-monolitico
#docker login